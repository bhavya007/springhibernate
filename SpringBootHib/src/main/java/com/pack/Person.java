package com.pack;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("unused")
@Entity
@Table(name="person")
public class Person {
@Id
@GeneratedValue(strategy =GenerationType.IDENTITY)
private Long id;
@Column
private String name;
@Column
private String city;
public Long getId() {
return id;
}
public void setId(Long id) {
this.id = id;
}
public String getName() {
return name;
}
public void setName(String name) {
this.name = name;
}
public String getCity() {
return city;
}
public void setCity(String city) {
this.city = city;
}
public Person(Long id, String name, String city) {
super();
this.id = id;
this.name = name;
this.city = city;
}
public Person() {
super();
// TODO Auto-generated constructor stub
}
}