package com.pack;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PersonDao {
@Autowired
SessionFactory sessionFactory;
	
	public List<Person> getAllPersons() {
		@SuppressWarnings("unchecked")
		List<Person> li=sessionFactory.getCurrentSession().createQuery("from Person").list();
		return li;
	}

	public void savePerson(Person person) {
		sessionFactory.getCurrentSession().save(person);
	}

	public void delete(Person person) {
		sessionFactory.getCurrentSession().delete(person);
	}

}
